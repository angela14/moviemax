import java.util.Properties

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    kotlin("kapt")
}

android {
    namespace = "com.example.moviemax"
    compileSdk = 33

    defaultConfig {
        applicationId = "com.example.moviemax"
        minSdk = 24
        targetSdk = 33
        versionCode = 1
        versionName = "1.0"

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        getByName("debug") {}
        getByName("release") {}
    }

    buildFeatures {
        buildConfig = true
    }

    flavorDimensions("flavor")

    val secretFile = file("secret.properties")
    val secretProperties = Properties()
    secretProperties.load(secretFile.inputStream())

    val endpointFile = file("endpoint.properties")
    val endpointProperties = Properties()
    endpointProperties.load(endpointFile.inputStream())

    productFlavors {
        create("development") {
            buildConfigField(
                "String",
                "AUTHENTICATION_TOKEN",
                secretProperties["AUTHENTICATION_TOKEN"].toString()
            )
            buildConfigField(
                "String",
                "ENDPOINT_URL",
                endpointProperties["ENDPOINT_URL"].toString()
            )
            buildConfigField(
                "String",
                "IMAGE_URL",
                endpointProperties["ENDPOINT_IMAGE_URL_w200"].toString()
            )
            buildConfigField(
                "String",
                "IMAGE_URL_LARGE",
                endpointProperties["ENDPOINT_IMAGE_URL_w300"].toString()
            )
        }
        create("production") {
            buildConfigField(
                "String",
                "AUTHENTICATION_TOKEN",
                secretProperties["AUTHENTICATION_TOKEN"].toString()
            )
            buildConfigField(
                "String",
                "IMAGE_URL",
                endpointProperties["ENDPOINT_IMAGE_URL_w200"].toString()
            )
            buildConfigField(
                "String",
                "IMAGE_URL_LARGE",
                endpointProperties["ENDPOINT_IMAGE_URL_w300"].toString()
            )
        }
    }


    android {
        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_11
            targetCompatibility = JavaVersion.VERSION_11
        }
    }

    kotlinOptions {
        jvmTarget = "11"
    }


    viewBinding {
        enable = true
    }
}

dependencies {
    implementation("androidx.core:core-ktx:1.9.0")
    implementation("com.google.android.material:material:1.8.0")

    testImplementation("junit:junit:4.13.2")
    androidTestImplementation("androidx.test.ext:junit:1.1.5")
    androidTestImplementation("androidx.test.espresso:espresso-core:3.5.1")

    // View
    implementation("androidx.appcompat:appcompat:1.6.1")
    implementation("androidx.recyclerview:recyclerview:1.2.0")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("androidx.constraintlayout:constraintlayout:2.1.4")
    implementation("androidx.legacy:legacy-support-v4:1.0.0")

    // Retrofit
    implementation("com.squareup.retrofit2:retrofit:2.7.1")
    implementation("com.squareup.retrofit2:retrofit-converters:2.6.1")
    implementation("com.squareup.retrofit2:retrofit-adapters:2.6.1")
    implementation("com.squareup.retrofit2:adapter-rxjava3:2.9.0")
    implementation("com.squareup.retrofit2:converter-gson:2.6.1")
    implementation("com.squareup.okhttp3:logging-interceptor:4.3.1")

    // Basic Dagger 2 (required)
    implementation("com.google.dagger:dagger:2.24")
    kapt("com.google.dagger:dagger-compiler:2.24")
    kapt("com.google.dagger:dagger-android-processor:2.24")

    // ViewModel and LiveData
    implementation("androidx.lifecycle:lifecycle-common:2.3.1")
    implementation("androidx.lifecycle:lifecycle-runtime-ktx:2.3.1")
    implementation("androidx.lifecycle:lifecycle-extensions:2.2.0")
    implementation("androidx.lifecycle:lifecycle-livedata:2.2.0")

    //    glide
    implementation("com.github.bumptech.glide:glide:4.11.0")
    annotationProcessor("com.github.bumptech.glide:compiler:4.11.0")

    // rx
    implementation("io.reactivex.rxjava3:rxandroid:3.0.0")

    // youtube
    implementation("com.pierfrancescosoffritti.androidyoutubeplayer:core:12.1.0")
}
