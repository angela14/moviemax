package com.example.moviemax.domain.repository

import com.example.moviemax.domain.data.MovieDetailResponse
import com.example.moviemax.domain.data.MovieGenreResponse
import com.example.moviemax.domain.data.MovieListResponse
import com.example.moviemax.domain.data.MovieReviewResponse
import com.example.moviemax.domain.data.MovieVideoResponse
import io.reactivex.rxjava3.core.Observable

abstract class MovieRepository {
    abstract fun getMovieList(page: Int, genre: String? = null): Observable<MovieListResponse>
    abstract fun getMovieGenre(): Observable<MovieGenreResponse>
    abstract fun getMovieDetail(movieId: Int): Observable<MovieDetailResponse>
    abstract fun getMovieReview(movieId: Int, page: Int): Observable<MovieReviewResponse>
    abstract fun getMovieVideo(movieId: Int): Observable<MovieVideoResponse>
}