package com.example.moviemax.domain.data

import com.google.gson.annotations.SerializedName

data class MovieGenreResponse(
    @SerializedName("genres")
    val genre: List<MovieGenre>
)

data class MovieGenre(
    val id: Int,
    val name: String,
    val icon: Int
)