package com.example.moviemax.domain.repository

import com.example.moviemax.BaseApplication
import com.example.moviemax.di.AppComponent
import com.example.moviemax.domain.data.MovieDetailResponse
import com.example.moviemax.domain.data.MovieGenreResponse
import com.example.moviemax.domain.data.MovieReviewResponse
import com.example.moviemax.domain.data.MovieVideoResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.Retrofit
import javax.inject.Inject

class MovieRepositoryImpl : MovieRepository() {
    @Inject
    lateinit var retrofit: Retrofit
    private var apiService: APIService

    init {
        val appComponent: AppComponent = BaseApplication().getAPIComponent()
        appComponent.inject(this)
        apiService = retrofit.create(APIService::class.java)
    }

    override fun getMovieList(page: Int, genre: String?) = apiService.getMovieList(page, genre)

    override fun getMovieGenre(): Observable<MovieGenreResponse> = apiService.getMovieGenre()

    override fun getMovieDetail(movieId: Int): Observable<MovieDetailResponse> =
        apiService.getMovieDetail(movieId)

    override fun getMovieReview(movieId: Int, page: Int): Observable<MovieReviewResponse> =
        apiService.getMovieReviews(movieId, page)

    override fun getMovieVideo(movieId: Int): Observable<MovieVideoResponse> =
        apiService.getMovieVideos(movieId)
}