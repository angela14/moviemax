package com.example.moviemax.domain.usecase

import com.example.moviemax.domain.data.MovieListResponse
import com.example.moviemax.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable

class GetMovieListUseCase(private val movieRepository: MovieRepository) {
    fun getMovieList(page: Int, genre: String?): Observable<MovieListResponse> =
        movieRepository.getMovieList(page, genre)
}