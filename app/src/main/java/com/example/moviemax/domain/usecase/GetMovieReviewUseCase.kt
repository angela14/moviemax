package com.example.moviemax.domain.usecase

import com.example.moviemax.domain.data.MovieReviewResponse
import com.example.moviemax.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable

class GetMovieReviewUseCase(private val movieRepository: MovieRepository) {
    fun getMovieReview(movieId: Int, page: Int): Observable<MovieReviewResponse> =
        movieRepository.getMovieReview(movieId, page)
}