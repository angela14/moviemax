package com.example.moviemax.domain.usecase

import com.example.moviemax.domain.data.MovieGenreResponse
import com.example.moviemax.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable

class GetMovieGenreUseCase(private val movieRepository: MovieRepository) {
    fun getMovieGenre(): Observable<MovieGenreResponse> = movieRepository.getMovieGenre()
}