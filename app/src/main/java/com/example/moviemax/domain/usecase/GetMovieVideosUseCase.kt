package com.example.moviemax.domain.usecase

import com.example.moviemax.domain.data.MovieVideoResponse
import com.example.moviemax.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable

class GetMovieVideosUseCase(private val movieRepository: MovieRepository) {
    fun getMovieVideos(movieId: Int): Observable<MovieVideoResponse> =
        movieRepository.getMovieVideo(movieId)
}