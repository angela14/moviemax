package com.example.moviemax.domain.usecase

import com.example.moviemax.domain.data.MovieDetailResponse
import com.example.moviemax.domain.repository.MovieRepository
import io.reactivex.rxjava3.core.Observable

class GetMovieDetailUseCase(private val movieRepository: MovieRepository) {
    fun getMovieDetail(movieId: Int): Observable<MovieDetailResponse> =
        movieRepository.getMovieDetail(movieId)
}