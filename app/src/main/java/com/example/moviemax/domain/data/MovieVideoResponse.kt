package com.example.moviemax.domain.data

import com.google.gson.annotations.SerializedName

data class MovieVideoResponse(
    @SerializedName("results")
    val result: List<MovieVideo>
)

data class MovieVideo(
    val type: String,
    val site: String,
    val key: String,
    val official: Boolean
)