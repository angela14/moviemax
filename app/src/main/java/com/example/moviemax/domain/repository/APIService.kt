package com.example.moviemax.domain.repository

import com.example.moviemax.domain.data.MovieDetailResponse
import com.example.moviemax.domain.data.MovieGenreResponse
import com.example.moviemax.domain.data.MovieListResponse
import com.example.moviemax.domain.data.MovieReviewResponse
import com.example.moviemax.domain.data.MovieVideoResponse
import io.reactivex.rxjava3.core.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface APIService {
    @GET("/3/discover/movie")
    fun getMovieList(
        @Query("page") page: Int,
        @Query("with_genres") genre: String? = null
    ): Observable<MovieListResponse>

    @GET("/3/genre/movie/list")
    fun getMovieGenre(@Query("language") language: String? = "en"): Observable<MovieGenreResponse>

    @GET("/3/movie/{movie_id}")
    fun getMovieDetail(@Path("movie_id") id: Int): Observable<MovieDetailResponse>

    @GET("/3/movie/{movie_id}/reviews")
    fun getMovieReviews(
        @Path("movie_id") id: Int,
        @Query("page") page: Int
    ): Observable<MovieReviewResponse>

    @GET("/3/movie/{movie_id}/videos")
    fun getMovieVideos(@Path("movie_id") id: Int): Observable<MovieVideoResponse>

}