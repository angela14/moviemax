package com.example.moviemax.presentation.adapter

import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviemax.BuildConfig
import com.example.moviemax.R
import com.example.moviemax.databinding.ItemLoadingBinding
import com.example.moviemax.databinding.ItemMovieReviewBinding
import com.example.moviemax.domain.data.ReviewResults
import com.example.moviemax.shared.DateUtils

class MovieReviewPaginationAdapter : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var list = ArrayList<ReviewResults?>()
    private val LOADING = 0
    private val ITEM = 1
    private var isLoadingAdded = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null

        when (viewType) {
            ITEM -> {
                val binding = ItemMovieReviewBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                viewHolder = MovieReviewViewHolder(binding)
            }

            LOADING -> {
                val binding = ItemLoadingBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                viewHolder = ItemLoadingViewHolder(binding)
            }
        }

        return viewHolder!!
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size - 1 && isLoadingAdded && list[position] == null) LOADING else ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = list[position]
        when (getItemViewType(position)) {
            ITEM -> {
                val movieViewHolder = holder as MovieReviewViewHolder
                movie?.let { movieViewHolder.bind(it) }
            }

            LOADING -> {
                val loadingViewHolder = holder as ItemLoadingViewHolder
                loadingViewHolder.bind()
            }
        }

    }

    fun addLoadingFooter() {
        if (!isLoadingAdded) {
            isLoadingAdded = true
            list.add(null)
            notifyItemInserted(list.size - 1)
        }
    }

    fun removeLoadingFooter() {
        if (isLoadingAdded) {
            isLoadingAdded = false
            val position = list.size - 1
            if (position >= 0 && list[position] == null) {
                list.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    fun addAll(reviewResult: List<ReviewResults?>) {
        list.addAll(reviewResult)
        notifyDataSetChanged()
    }


    inner class MovieReviewViewHolder(private val binding: ItemMovieReviewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ReviewResults) {
            binding.apply {
                item.authorDetail.let { avatar ->
                    Glide.with(root.context)
                        .load("${BuildConfig.IMAGE_URL}${avatar.avatarPath}")
                        .placeholder(ContextCompat.getDrawable(root.context, R.drawable.ic_circle_grey))
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(ivAvatar)
                }
                tvUsername.text = item.authorDetail.username?:"-"
                tvContent.text = item.content
                tvRating.text = "${item.authorDetail.rating?:0.0}"
                tvUpdatedAt.text = DateUtils.formatToDDMMMYYYY(item.updatedAt)
            }
        }
    }

    inner class ItemLoadingViewHolder(private val binding: ItemLoadingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.apply {
                clLoadMore.visibility = View.VISIBLE
                progressBar.backgroundTintList =
                    ColorStateList.valueOf(ContextCompat.getColor(root.context, R.color.white))
                tvLoading.setTextColor(ContextCompat.getColor(root.context, R.color.white))
            }
        }
    }

}