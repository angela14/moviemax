package com.example.moviemax.presentation.movielist

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.moviemax.BaseApplication
import com.example.moviemax.base.BaseActivity
import com.example.moviemax.base.BindData
import com.example.moviemax.databinding.ActivityMovieListBinding
import com.example.moviemax.presentation.adapter.MovieListPaginationAdapter
import com.example.moviemax.presentation.moviedetail.MovieDetailActivity
import com.example.moviemax.factory.MovieViewModelFactory
import javax.inject.Inject

class MovieListActivity : BaseActivity<ActivityMovieListBinding>(), BindData {
    @Inject
    lateinit var movieListViewModelFactory: MovieViewModelFactory

    private lateinit var movieViewModel: MovieViewModel
    private lateinit var paginationAdapter: MovieListPaginationAdapter
    private var page = 1
    private var totalPages = 1
    private var isNextPage = false
    private var isLoadingData = false // Initialize this variable in your class

    override val bindingInflater: (LayoutInflater) -> ActivityMovieListBinding
        get() = ActivityMovieListBinding::inflate


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.apply {
            movieListToolbar.tvTitle.text = intent?.getStringExtra(GENRE)
            movieListToolbar.ivBack.setOnClickListener {
                finish()
            }
        }
        initViewModel()
        setupAdapter()
        setupRecyclerView()
        fetchData()
        bindDataToView()
    }

    override fun initViewModel() {
        BaseApplication().getAPIComponent().inject(this@MovieListActivity)
        movieViewModel =
            ViewModelProvider(
                this@MovieListActivity,
                movieListViewModelFactory
            )[MovieViewModel::class.java]
    }

    private fun setupAdapter() {
        paginationAdapter = MovieListPaginationAdapter() {
            MovieDetailActivity.start(this@MovieListActivity, it, intent?.getStringExtra(GENRE)?:"")
        }
        binding.let {
            it.rvMovieList.setHasFixedSize(true)
            it.rvMovieList.setItemViewCacheSize(paginationAdapter.itemCount)
            it.rvMovieList.adapter = paginationAdapter
        }
    }

    private fun fetchData() {
        if (page > totalPages) return
        movieViewModel.fetchMovieList(page, intent?.getStringExtra(GENRE_ID))
    }

    override fun bindDataToView() {
        movieViewModel.movieListLiveData.observe(this) { list ->
            if (!isNextPage) {
                paginationAdapter.addAll(list.results)
                if (page <= totalPages) paginationAdapter.addLoadingFooter()
                totalPages = list.total_pages

                if (page == totalPages) paginationAdapter.removeLoadingFooter()
            } else {
                Handler().postDelayed({
                    paginationAdapter.removeLoadingFooter()
                    isLoadingData = false
                    paginationAdapter.addAll(list.results)

                    if (page != totalPages) paginationAdapter.addLoadingFooter()
                    else paginationAdapter.removeLoadingFooter()
                },1000)
            }
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.let {
            it.rvMovieList.layoutManager = layoutManager
            it.svMovieList.viewTreeObserver.addOnScrollChangedListener {
                val scrollY = it.svMovieList.scrollY
                val contentViewHeight = it.svMovieList.getChildAt(0).height
                val scrollViewHeight = it.svMovieList.height

                val scrolledToBottom = scrollY + scrollViewHeight >= contentViewHeight

                if (scrolledToBottom && !isLoadingData) {
                    page++
                    isLoadingData = true
                    isNextPage = true
                    fetchData()
                }
            }
        }
    }

    companion object {
        private const val GENRE_ID = "genre_id"
        private const val GENRE = "genre"

        fun start(context: Context, genreId: String, genre: String) {
            val intent = Intent(context, MovieListActivity::class.java).apply {
                putExtra(GENRE_ID, genreId)
                putExtra(GENRE, genre)
            }
            context.startActivity(intent)
        }
    }
}