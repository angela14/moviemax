package com.example.moviemax.presentation.videoplayer

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import androidx.annotation.NonNull
import androidx.lifecycle.ViewModelProvider
import com.example.moviemax.BaseApplication
import com.example.moviemax.base.BaseActivity
import com.example.moviemax.base.BindData
import com.example.moviemax.databinding.ActivityVideoPlayerBinding
import com.example.moviemax.factory.MovieViewModelFactory
import com.example.moviemax.factory.VideoPlayerViewModelFactory
import com.example.moviemax.presentation.moviedetail.MovieDetailViewModel
import com.example.moviemax.presentation.movielist.MovieViewModel
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener
import javax.inject.Inject

class VideoPlayerActivity : BaseActivity<ActivityVideoPlayerBinding>(), BindData {
    @Inject
    lateinit var videoPlayerViewModelFactory: VideoPlayerViewModelFactory

    private lateinit var videoPlayerViewModel: VideoPlayerViewModel

    override val bindingInflater: (LayoutInflater) -> ActivityVideoPlayerBinding
        get() = ActivityVideoPlayerBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.videoPlayerToolbar.ivBack.setOnClickListener { finish() }
        initViewModel()
        fetchData()
        bindDataToView()
    }

    override fun initViewModel() {
        BaseApplication().getAPIComponent().inject(this@VideoPlayerActivity)
        videoPlayerViewModel =
            ViewModelProvider(
                this@VideoPlayerActivity,
                videoPlayerViewModelFactory
            )[VideoPlayerViewModel::class.java]
    }

    private fun fetchData() {
        videoPlayerViewModel.fetchMovieVideos(intent?.getIntExtra(MOVIE_ID,0)?:0)
    }

    override fun bindDataToView() {
        videoPlayerViewModel.movieTrailerLiveData.observe(this@VideoPlayerActivity) {
            binding.youtubePlayerView.addYouTubePlayerListener(
                object : AbstractYouTubePlayerListener() {
                    override fun onReady(@NonNull youTubePlayer: YouTubePlayer) {
                        youTubePlayer.loadVideo(it, 0f)
                        youTubePlayer.play()
                    }
                }
            )
        }
    }

    companion object {
        private const val MOVIE_ID = "movie_id"

        fun start(context: Context, movieId: Int) {
            val intent = Intent(context, VideoPlayerActivity::class.java).apply {
                putExtra(MOVIE_ID, movieId)
            }
            context.startActivity(intent)
        }
    }

}