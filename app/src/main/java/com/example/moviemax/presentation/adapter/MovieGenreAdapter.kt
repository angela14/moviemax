package com.example.moviemax.presentation.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView.ViewHolder
import com.example.moviemax.databinding.ItemMovieGenreBinding
import com.example.moviemax.domain.data.MovieGenre

class MovieGenreAdapter(private val onItemClicked: ((genre: MovieGenre) -> Unit)? = null) :
    ListAdapter<MovieGenre, ViewHolder>(DIFF_CALLBACK) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieGenreViewHolder {
        val binding = ItemMovieGenreBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return MovieGenreViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val viewHolder = holder as MovieGenreViewHolder
        getItem(position)?.let { item ->
            viewHolder.bind(item)
        }
    }

    inner class MovieGenreViewHolder(private val binding: ItemMovieGenreBinding) :
        ViewHolder(binding.root) {

        fun bind(item: MovieGenre) {

            binding.tvGenre.text = item.name
            binding.ivGenreIcon.setImageDrawable(ContextCompat.getDrawable(binding.root.context, item.icon))

            binding.root.setOnClickListener {
                onItemClicked?.invoke(item)
            }
        }
    }

    companion object {
        private val DIFF_CALLBACK =
            object : DiffUtil.ItemCallback<MovieGenre>() {
                override fun areItemsTheSame(
                    oldItem: MovieGenre,
                    newItem: MovieGenre
                ): Boolean = oldItem.id == newItem.id

                override fun areContentsTheSame(
                    oldItem: MovieGenre,
                    newItem: MovieGenre
                ): Boolean = oldItem == newItem
            }
    }
}