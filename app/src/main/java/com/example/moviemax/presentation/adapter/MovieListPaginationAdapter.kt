package com.example.moviemax.presentation.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DecodeFormat
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.example.moviemax.BuildConfig
import com.example.moviemax.R
import com.example.moviemax.databinding.ItemLoadingBinding
import com.example.moviemax.databinding.ItemMovieListBinding
import com.example.moviemax.domain.data.MovieList
import com.example.moviemax.shared.DateUtils

class MovieListPaginationAdapter (private val onItemClicked: ((movieId: Int) -> Unit)): RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private var list = ArrayList<MovieList?>()
    private val LOADING = 0
    private val ITEM = 1
    private var isLoadingAdded = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        var viewHolder: RecyclerView.ViewHolder? = null

        when (viewType) {
            ITEM -> {
                val binding = ItemMovieListBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                viewHolder = MovieListViewHolder(binding)
            }

            LOADING -> {
                val binding = ItemLoadingBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
                viewHolder = ItemLoadingViewHolder(binding)
            }
        }

        return viewHolder!!
    }

    override fun getItemCount(): Int = list.size

    override fun getItemViewType(position: Int): Int {
        return if (position == list.size - 1 && isLoadingAdded && list[position] == null) LOADING else ITEM
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val movie = list[position]
        when (getItemViewType(position)) {
            ITEM -> {
                val movieViewHolder = holder as MovieListViewHolder
                movie?.let { movieViewHolder.bind(it) }
            }

            LOADING -> {
                val loadingViewHolder = holder as ItemLoadingViewHolder
                loadingViewHolder.bind()
            }
        }

    }

    fun addLoadingFooter() {
        if (!isLoadingAdded) {
            isLoadingAdded = true
            list.add(null)
            notifyItemInserted(list.size - 1)
        }
    }

    fun removeLoadingFooter() {
        if (isLoadingAdded) {
            isLoadingAdded = false
            val position = list.size - 1
            if (position >= 0 && list[position] == null) {
                list.removeAt(position)
                notifyItemRemoved(position)
            }
        }
    }

    fun addAll(moveResults: List<MovieList?>) {
        list.addAll(moveResults)
        notifyDataSetChanged()
    }


    inner class MovieListViewHolder(private val binding: ItemMovieListBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(item: MovieList) {
            binding.apply {
                item.posterPath?.let { poster ->
                    Glide.with(binding.root.context)
                        .load("${BuildConfig.IMAGE_URL}/${poster}")
                        .placeholder(ContextCompat.getDrawable(root.context, R.drawable.no_image))
                        .apply(RequestOptions()
                            .format(DecodeFormat.PREFER_RGB_565)
                            .override(150, 200))
                        .centerInside()
                        .diskCacheStrategy(DiskCacheStrategy.NONE)
                        .into(ivImagePoster)
                }
                tvMovieTitle.text = item.originalTitle
                tvRating.text = "${item.rating}"
                tvMovieDesc.text = item.overview
                tvReleaseDate.text = DateUtils.formatToDDMMMYYYY(item.releaseDate)

                root.setOnClickListener {
                    onItemClicked.invoke(item.id)
                }
            }
        }
    }

    inner class ItemLoadingViewHolder(private val binding: ItemLoadingBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind() {
            binding.clLoadMore.visibility = View.VISIBLE
        }
    }

}