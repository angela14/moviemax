package com.example.moviemax.presentation.moviedetail

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviemax.domain.data.MovieDetailResponse
import com.example.moviemax.domain.data.MovieReviewResponse
import com.example.moviemax.domain.usecase.GetMovieDetailUseCase
import com.example.moviemax.domain.usecase.GetMovieReviewUseCase
import com.example.moviemax.shared.LoadingState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MovieDetailViewModel @Inject constructor(
    private var getMovieDetailUseCase: GetMovieDetailUseCase,
    private var getMovieReviewUseCase: GetMovieReviewUseCase
) : ViewModel() {
    val movieDetailLiveData = MutableLiveData<MovieDetailResponse>()
    val movieReviewLiveData = MutableLiveData<MovieReviewResponse>()
    val loadingState = MutableLiveData(LoadingState.IDLE)

    @SuppressLint("CheckResult")
    fun fetchMovieDetail(movieId: Int) {
        loadingState.value = LoadingState.LOADING
        getMovieDetailUseCase.getMovieDetail(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                movieDetailLiveData.value = it
                loadingState.value = LoadingState.SUCCESS
            }, {
                loadingState.value = LoadingState.ERROR
            })
    }

    @SuppressLint("CheckResult")
    fun fetchMovieReview(movieId: Int, page: Int) {
        getMovieReviewUseCase.getMovieReview(movieId, page)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                movieReviewLiveData.value = it
            }, {})
    }
}