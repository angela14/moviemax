package com.example.moviemax.presentation.homepage

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.example.moviemax.BaseApplication
import com.example.moviemax.base.BaseActivity
import com.example.moviemax.base.BindData
import com.example.moviemax.databinding.ActivityMainBinding
import com.example.moviemax.presentation.movielist.MovieListActivity
import com.example.moviemax.presentation.adapter.MovieGenreAdapter
import com.example.moviemax.shared.LoadingStateHelper.Companion.isLoading
import com.example.moviemax.factory.GenreViewModelFactory
import javax.inject.Inject

class MainActivity : BaseActivity<ActivityMainBinding>(), BindData {
    @Inject
    lateinit var genreViewModelFactory: GenreViewModelFactory

    private lateinit var genreViewModel: GenreViewModel
    private lateinit var movieGenreAdapter: MovieGenreAdapter

    override val bindingInflater: (LayoutInflater) -> ActivityMainBinding
        get() = ActivityMainBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding.ivBack.setOnClickListener { finishAffinity() }
        initViewModel()
        initView()
        fetchData()
        bindDataToView()
    }

    private fun fetchData() {
        genreViewModel.fetchMovieGenre()
    }

    override fun bindDataToView() {
        genreViewModel.movieGenreLiveData.observe(this) { genre ->
            genre?.apply {
                movieGenreAdapter.submitList(this)
            }
        }

        genreViewModel.loadingState.observe(this) { state ->
            if (!state.isLoading()) {
                binding.pbGenre.visibility = View.GONE
            }
        }
    }


    override fun initViewModel() {
        BaseApplication().getAPIComponent().inject(this@MainActivity)
        genreViewModel =
            ViewModelProvider(this@MainActivity, genreViewModelFactory)[GenreViewModel::class.java]
    }

    private fun initView() {
        Glide.with(this@MainActivity)
            .load("https://m.media-amazon.com/images/I/71xDtUSyAKL._AC_SL1500_.jpg")
            .centerCrop()
            .into(binding.ivBackground)
        movieGenreAdapter = MovieGenreAdapter { item ->
            MovieListActivity.start(this@MainActivity, "${item.id}", item.name)
        }

        binding.rvMovieGenre.apply {
            layoutManager = GridLayoutManager(this@MainActivity, 2)
            adapter = movieGenreAdapter
            isNestedScrollingEnabled = false
        }
    }
}