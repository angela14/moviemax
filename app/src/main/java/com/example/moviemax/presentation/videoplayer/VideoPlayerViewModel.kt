package com.example.moviemax.presentation.videoplayer

import android.annotation.SuppressLint
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviemax.domain.usecase.GetMovieVideosUseCase
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class VideoPlayerViewModel @Inject constructor(
    private var getMovieVideosUseCase: GetMovieVideosUseCase
) : ViewModel() {
    val movieTrailerLiveData = MutableLiveData<String>()

    @SuppressLint("CheckResult")
    fun fetchMovieVideos(movieId: Int) {
        getMovieVideosUseCase.getMovieVideos(movieId)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                val videoKey = it.result.find { vid ->
                    vid.site.equals(
                        "youtube",
                        true
                    ) && vid.official && vid.type.equals("trailer", true)
                }?.key ?: ""
                movieTrailerLiveData.value = videoKey
            }, {

            })
    }
}