package com.example.moviemax.presentation.moviedetail

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.moviemax.BaseApplication
import com.example.moviemax.BuildConfig
import com.example.moviemax.base.BaseActivity
import com.example.moviemax.base.BindData
import com.example.moviemax.databinding.ActivityMovieDetailBinding
import com.example.moviemax.presentation.adapter.MovieReviewPaginationAdapter
import com.example.moviemax.presentation.videoplayer.VideoPlayerActivity
import com.example.moviemax.shared.DateUtils
import com.example.moviemax.shared.LoadingStateHelper.Companion.isLoading
import com.example.moviemax.factory.MovieDetailViewModelFactory
import javax.inject.Inject

class MovieDetailActivity : BaseActivity<ActivityMovieDetailBinding>(), BindData {
    @Inject
    lateinit var movieDetailViewModelFactory: MovieDetailViewModelFactory

    private lateinit var movieDetailViewModel: MovieDetailViewModel

    private lateinit var paginationAdapter: MovieReviewPaginationAdapter

    private var page = 1
    private var totalPages = 1
    private var isNextPage = false
    private var isLoadingData = false
    private var movieId: Int = 0

    override val bindingInflater: (LayoutInflater) -> ActivityMovieDetailBinding
        get() = ActivityMovieDetailBinding::inflate

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        movieId = intent?.getIntExtra(MOVIE_ID, 0) ?: 0
        binding.movieDetailToolbar.ivBack.setOnClickListener { finish() }
        binding.btnTrailer.setOnClickListener {
            VideoPlayerActivity.start(this@MovieDetailActivity, movieId)
        }
        setupAdapter()
        setupRecyclerView()
        initViewModel()
        fetchData()
        fetchReviewData()
        bindDataToView()
    }

    override fun bindDataToView() {
        movieDetailViewModel.movieDetailLiveData.observe(this) {
            Glide.with(this@MovieDetailActivity)
                .load("${BuildConfig.IMAGE_URL_LARGE}${it.backdropPath}")
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .into(binding.ivBackDrop)
            binding.apply {
                tvMovieTitle.text = it.originalTitle
                movieDetailToolbar.tvTitle.text = intent.getStringExtra(GENRE)
                tvReleaseDate.text = DateUtils.formatToDDMMMYYYY(it.releaseDate)
                tvDuration.text = "${it.runtime} Minutes"
                tvMovieOverview.text = it.overview
            }
        }

        movieDetailViewModel.movieReviewLiveData.observe(this) { review ->
            binding.apply {
                rvMovieReview.visibility = if (review.results.isEmpty()) View.GONE else View.VISIBLE
                clNoReviews.visibility = if (review.results.isEmpty()) View.VISIBLE else View.GONE
            }

            if (!isNextPage) {
                paginationAdapter.addAll(review.results)
                if (page <= totalPages) paginationAdapter.addLoadingFooter()
                totalPages = review.totalPage

                if (page == totalPages) paginationAdapter.removeLoadingFooter()
            } else {
                Handler().postDelayed({
                    paginationAdapter.removeLoadingFooter()
                    isLoadingData = false
                    paginationAdapter.addAll(review.results)

                    if (page != totalPages) paginationAdapter.addLoadingFooter()
                    else paginationAdapter.removeLoadingFooter()
                }, 1000)
            }
        }

        movieDetailViewModel.loadingState.observe(this@MovieDetailActivity) { it ->
            if (!it.isLoading()) binding.pbMovieDetail.visibility = View.GONE
        }
    }

    private fun setupAdapter() {
        paginationAdapter = MovieReviewPaginationAdapter()
        binding.let {
            it.rvMovieReview.setHasFixedSize(true)
            it.rvMovieReview.adapter = paginationAdapter
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.let {
            it.rvMovieReview.layoutManager = layoutManager
            it.svMovieReview.viewTreeObserver.addOnScrollChangedListener {
                val scrollY = it.svMovieReview.scrollY
                val contentViewHeight = it.svMovieReview.getChildAt(0).height
                val scrollViewHeight = it.svMovieReview.height

                val scrolledToBottom = scrollY + scrollViewHeight >= contentViewHeight

                if (scrolledToBottom && !isLoadingData) {
                    page++
                    isLoadingData = true
                    isNextPage = true
                    fetchReviewData()
                }
            }
        }
    }

    override fun initViewModel() {
        BaseApplication().getAPIComponent().inject(this@MovieDetailActivity)
        movieDetailViewModel =
            ViewModelProvider(
                this@MovieDetailActivity,
                movieDetailViewModelFactory
            )[MovieDetailViewModel::class.java]
    }

    private fun fetchData() {
        movieDetailViewModel.fetchMovieDetail(movieId)
    }

    private fun fetchReviewData() {
        if (page > totalPages) return
        movieDetailViewModel.fetchMovieReview(movieId, page)
    }


    companion object {
        private const val MOVIE_ID = "movie_id"
        private const val GENRE = "genre"

        fun start(context: Context, movieId: Int, genre: String) {
            val intent = Intent(context, MovieDetailActivity::class.java).apply {
                putExtra(MOVIE_ID, movieId)
                putExtra(GENRE, genre)
            }
            context.startActivity(intent)
        }
    }
}