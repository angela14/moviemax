package com.example.moviemax.presentation.movielist

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviemax.domain.data.MovieListResponse
import com.example.moviemax.domain.usecase.GetMovieListUseCase
import com.example.moviemax.shared.LoadingState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class MovieViewModel @Inject constructor(
    private var getMovieListUseCase: GetMovieListUseCase,
) : ViewModel() {
    val movieListLiveData = MutableLiveData<MovieListResponse>()
    private val loadingState = MutableLiveData(LoadingState.IDLE)

    @SuppressLint("CheckResult")
    fun fetchMovieList(page: Int, genre: String?) {
        loadingState.value = LoadingState.LOADING
        getMovieListUseCase.getMovieList(page, genre)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                movieListLiveData.value = it
                loadingState.value = LoadingState.SUCCESS
            }, {
                loadingState.value = LoadingState.ERROR
            })
    }
}