package com.example.moviemax.presentation.homepage

import android.annotation.SuppressLint
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.moviemax.R
import com.example.moviemax.domain.data.MovieGenre
import com.example.moviemax.domain.usecase.GetMovieGenreUseCase
import com.example.moviemax.shared.LoadingState
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers
import javax.inject.Inject

class GenreViewModel @Inject constructor(
    private var getMovieGenreUseCase: GetMovieGenreUseCase
): ViewModel() {
    val movieGenreLiveData = MutableLiveData<List<MovieGenre>>()
    val loadingState = MutableLiveData(LoadingState.LOADING)

    private fun mapIcon(name: String): Int {
        return when(name.toLowerCase()) {
            "action" -> R.drawable.ic_gun
            "adventure" -> R.drawable.ic_hat
            "animation" -> R.drawable.ic_flower
            "comedy" -> R.drawable.ic_comedy
            "crime" -> R.drawable.ic_arrest
            "documentary" -> R.drawable.ic_video_camera
            "drama" -> R.drawable.ic_theater
            "family" -> R.drawable.ic_theater
            "fantasy" -> R.drawable.ic_fortress
            "history" -> R.drawable.ic_clock
            "horror" -> R.drawable.ic_pumpkin
            "music" -> R.drawable.ic_musical
            "mystery" -> R.drawable.ic_mystery
            "romance" -> R.drawable.ic_romance
            "science fiction" -> R.drawable.ic_alien
            "tv movie" -> R.drawable.ic_television
            "thriller" -> R.drawable.ic_knife
            "war" -> R.drawable.ic_tank
            "western" -> R.drawable.ic_western
            else ->  R.drawable.ic_gun
        }
    }

    @SuppressLint("CheckResult")
    fun fetchMovieGenre() {
        getMovieGenreUseCase.getMovieGenre()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe ({
                loadingState.value = LoadingState.SUCCESS
                val movieGenreWithIcon = it.genre.map { item ->
                    MovieGenre(item.id, item.name, mapIcon(item.name))
                }
                movieGenreLiveData.value = movieGenreWithIcon
            },{
                loadingState.value = LoadingState.ERROR
            })
    }

}