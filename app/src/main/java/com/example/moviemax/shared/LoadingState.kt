package com.example.moviemax.shared

enum class LoadingState {
    IDLE,
    LOADING,
    SUCCESS,
    ERROR
}