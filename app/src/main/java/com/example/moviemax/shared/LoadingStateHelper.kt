package com.example.moviemax.shared

class LoadingStateHelper {
    companion object {
        fun LoadingState.isLoading() = this == LoadingState.LOADING
    }
}