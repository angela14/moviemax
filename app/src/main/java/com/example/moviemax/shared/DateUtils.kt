package com.example.moviemax.shared

import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DateUtils {
    companion object {
        fun formatToDDMMMYYYY(date : String) : String {
            var input = SimpleDateFormat("yyyy-MM-dd")
            var dateFormat = SimpleDateFormat("dd MMM yyyy")
            val output = input.parse(date)
            return dateFormat.format(output)
        }
    }
}