package com.example.moviemax

import dagger.Module
import dagger.Provides

@Module
class AppModule constructor(baseApplication: BaseApplication){
    private var base : BaseApplication = baseApplication
    @Provides
    fun provideBaseApplication() : BaseApplication {
        return base
    }
}