package com.example.moviemax.base

interface BindData {
    fun initViewModel()
    fun bindDataToView()
}