package com.example.moviemax.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviemax.BaseApplication
import com.example.moviemax.domain.usecase.GetMovieListUseCase
import com.example.moviemax.presentation.movielist.MovieViewModel
import javax.inject.Inject

class MovieViewModelFactory @Inject constructor(
    private val getMovieListUseCase: GetMovieListUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        BaseApplication().getAPIComponent().inject(this)
        if (modelClass.isAssignableFrom(MovieViewModel::class.java)){
            return MovieViewModel(getMovieListUseCase) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}