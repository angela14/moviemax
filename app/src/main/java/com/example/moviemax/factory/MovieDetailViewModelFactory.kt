package com.example.moviemax.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviemax.BaseApplication
import com.example.moviemax.domain.usecase.GetMovieDetailUseCase
import com.example.moviemax.domain.usecase.GetMovieReviewUseCase
import com.example.moviemax.presentation.moviedetail.MovieDetailViewModel
import javax.inject.Inject

class MovieDetailViewModelFactory @Inject constructor(
    private val getMovieDetailUseCase: GetMovieDetailUseCase,
    private val getMovieReviewUseCase: GetMovieReviewUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        BaseApplication().getAPIComponent().inject(this)
        if (modelClass.isAssignableFrom(MovieDetailViewModel::class.java)) {
            return MovieDetailViewModel(getMovieDetailUseCase, getMovieReviewUseCase) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}