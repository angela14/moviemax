package com.example.moviemax.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviemax.BaseApplication
import com.example.moviemax.domain.usecase.GetMovieGenreUseCase
import com.example.moviemax.presentation.homepage.GenreViewModel
import javax.inject.Inject

class GenreViewModelFactory @Inject constructor(
    private val genreUseCase: GetMovieGenreUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        BaseApplication().getAPIComponent().inject(this)
        if (modelClass.isAssignableFrom(GenreViewModel::class.java)){
            return GenreViewModel(genreUseCase) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}