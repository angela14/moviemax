package com.example.moviemax.factory

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.moviemax.BaseApplication
import com.example.moviemax.domain.usecase.GetMovieVideosUseCase
import com.example.moviemax.presentation.videoplayer.VideoPlayerViewModel
import javax.inject.Inject

class VideoPlayerViewModelFactory @Inject constructor(
    private val getMovieVideosUseCase: GetMovieVideosUseCase
) : ViewModelProvider.Factory {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        BaseApplication().getAPIComponent().inject(this)
        if (modelClass.isAssignableFrom(VideoPlayerViewModel::class.java)){
            return VideoPlayerViewModel(getMovieVideosUseCase) as T
        }
        throw IllegalArgumentException("Unknown View Model Class")
    }
}