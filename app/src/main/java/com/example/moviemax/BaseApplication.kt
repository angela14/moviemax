package com.example.moviemax

import android.app.Application
import android.content.Context
import com.example.moviemax.di.AppComponent
import com.example.moviemax.di.ApiModule
import com.example.moviemax.di.DaggerAppComponent
import com.example.moviemax.di.UseCaseModule
import com.example.moviemax.di.ViewModelFactoryModule

class BaseApplication : Application(){
    companion object {
        var ctx: Context? = null
    }

    override fun onCreate() {
        super.onCreate()
        ctx = applicationContext
    }

    fun getAPIComponent(): AppComponent {
        return initDaggerComponent()
    }

    fun initDaggerComponent() : AppComponent {
        return DaggerAppComponent
            .builder()
            .apiModule(ApiModule(BuildConfig.ENDPOINT_URL))
            .useCaseModule(UseCaseModule())
            .viewModelFactoryModule(ViewModelFactoryModule())
            .build()
    }
}