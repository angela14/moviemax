package com.example.moviemax.di

import com.example.moviemax.domain.repository.MovieRepository
import com.example.moviemax.domain.usecase.GetMovieDetailUseCase
import com.example.moviemax.domain.usecase.GetMovieGenreUseCase
import com.example.moviemax.domain.usecase.GetMovieListUseCase
import com.example.moviemax.domain.usecase.GetMovieReviewUseCase
import com.example.moviemax.domain.usecase.GetMovieVideosUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    @Singleton
    fun provideGetMovieListUseCase(movieRepository: MovieRepository): GetMovieListUseCase {
        return GetMovieListUseCase(movieRepository)
    }

    @Provides
    @Singleton
    fun provideGetMovieGenreUseCase(movieRepository: MovieRepository): GetMovieGenreUseCase {
        return GetMovieGenreUseCase(movieRepository)
    }

    @Provides
    @Singleton
    fun provideGetMovieDetailUseCase(movieRepository: MovieRepository): GetMovieDetailUseCase {
        return GetMovieDetailUseCase(movieRepository)
    }

    @Provides
    @Singleton
    fun provideMovieReviewUseCase(movieRepository: MovieRepository): GetMovieReviewUseCase {
        return GetMovieReviewUseCase(movieRepository)
    }

    @Provides
    @Singleton
    fun provideMovieVideoUseCase(movieRepository: MovieRepository): GetMovieVideosUseCase {
        return GetMovieVideosUseCase(movieRepository)
    }
}