package com.example.moviemax.di

import com.example.moviemax.domain.usecase.GetMovieGenreUseCase
import com.example.moviemax.domain.usecase.GetMovieListUseCase
import com.example.moviemax.factory.GenreViewModelFactory
import com.example.moviemax.factory.MovieViewModelFactory
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ViewModelFactoryModule {

    @Provides
    @Singleton
    fun provideMovieViewModelFactory(
        getMovieListUseCase: GetMovieListUseCase,
    ): MovieViewModelFactory {
        return MovieViewModelFactory(getMovieListUseCase)
    }

    @Provides
    @Singleton
    fun provideGenreViewModelFactory(
        genreUseCase: GetMovieGenreUseCase,
    ): GenreViewModelFactory {
        return GenreViewModelFactory(genreUseCase)
    }
}