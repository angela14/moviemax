package com.example.moviemax.di

import com.example.moviemax.AppModule
import com.example.moviemax.presentation.homepage.MainActivity
import com.example.moviemax.domain.repository.MovieRepository
import com.example.moviemax.domain.repository.MovieRepositoryImpl
import com.example.moviemax.domain.usecase.GetMovieDetailUseCase
import com.example.moviemax.domain.usecase.GetMovieGenreUseCase
import com.example.moviemax.domain.usecase.GetMovieListUseCase
import com.example.moviemax.domain.usecase.GetMovieReviewUseCase
import com.example.moviemax.domain.usecase.GetMovieVideosUseCase
import com.example.moviemax.presentation.moviedetail.MovieDetailActivity
import com.example.moviemax.presentation.movielist.MovieListActivity
import com.example.moviemax.factory.GenreViewModelFactory
import com.example.moviemax.factory.MovieDetailViewModelFactory
import com.example.moviemax.factory.MovieViewModelFactory
import com.example.moviemax.factory.VideoPlayerViewModelFactory
import com.example.moviemax.presentation.videoplayer.VideoPlayerActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class, UseCaseModule::class, RepositoryModule::class, ViewModelFactoryModule::class])
interface AppComponent {
    fun inject(movieRepository: MovieRepository)
    fun inject(movieRepositoryImpl: MovieRepositoryImpl)

    // Activity
    fun inject(activity: MainActivity)
    fun inject(activity: MovieListActivity)
    fun inject(activity: MovieDetailActivity)
    fun inject(activity: VideoPlayerActivity)

    // ViewModel Factory
    fun inject(genreViewModelFactory: GenreViewModelFactory)
    fun inject(movieViewModelFactory: MovieViewModelFactory)
    fun inject(movieDetailViewModelFactory: MovieDetailViewModelFactory)
    fun inject(videoPlayerViewModelFactory: VideoPlayerViewModelFactory)

    // UseCase
    fun inject(movieListUseCase: GetMovieListUseCase)
    fun inject(movieGenreUseCase: GetMovieGenreUseCase)
    fun inject(movieDetailUseCase: GetMovieDetailUseCase)
    fun inject(movieReviewUseCase: GetMovieReviewUseCase)
    fun inject(movieVideosUseCase: GetMovieVideosUseCase)
}