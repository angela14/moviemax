package com.example.moviemax.di

import com.example.moviemax.domain.repository.MovieRepository
import com.example.moviemax.domain.repository.MovieRepositoryImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {
    @Singleton
    @Provides
    fun provideMovieListRepository(): MovieRepository {
        return MovieRepositoryImpl()
    }
}