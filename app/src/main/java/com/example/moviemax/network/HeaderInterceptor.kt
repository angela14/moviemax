package com.example.moviemax.network

import com.example.moviemax.BuildConfig
import okhttp3.Interceptor
import okhttp3.Response
import okhttp3.ResponseBody

class HeaderInterceptor : Interceptor {

    override fun intercept(chain: Interceptor.Chain): Response {
        val request = chain.request()
            .newBuilder()
            .header("Authorization", "Bearer ${BuildConfig.AUTHENTICATION_TOKEN}")
            .build()

        val response= chain.proceed(request)
        var stringData = ""
        if (response.code == 200 || response.code == 201){
            response.body?.string()?.let { json ->
                stringData = json
            }
        }
        val contentType = response.body?.contentType()
        val body = ResponseBody.create(contentType,stringData)
        return response.newBuilder().body(body).build()
    }
}