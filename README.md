Holla, this application contains movies that are around the world. This application is connected by TMBD API.
(https://www.themoviedb.org/documentation/api)

Feature: 

1. Filter the movies based on Genre
2. See movie list & detail
3. See Movie reviews
4. Watch movie trailer


**Languange :** Kotlin

**Architecture:** MVVM & Clean Architecture

**Library used (app) :**<br/>
Dagger2<br/>
Retrofit<br/>
LiveData<br/>
pierfrancescosoffritti.androidyoutubeplayer<br/>
RxJava3<br/>
Glide<br/>


## Screenshoot

- Genre List

[GenreList_1](https://postimg.cc/0M3f7DK7)

[GenreList_2](https://postimg.cc/G424nN5h)


- Movie List

[MovieList](https://postimg.cc/dLW8C8M3)


- Movie Detail

[MovieDetail](https://postimg.cc/mPYBqSb2)


- Movie Review

[MovieDetail](https://postimg.cc/JsLWbBbw)

- Video Player

[VideoPlayer](https://postimg.cc/GH00CsgB)
